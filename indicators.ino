void flashIndicator(int p, int c) {

  for (int i = 0; i < c; i++) {
    
    digitalWrite(p, HIGH);
    delay(20);
    digitalWrite(p, LOW);
    delay(40);
    
  }
  
}

void wifiIndicator() {

  if (wifiStatus == 0) {
    
    flashIndicator(WIFI_DOWN_PIN, 2);
    startWifi();
    
  } else if (wifiStatus == 4) {
    
    flashIndicator(WIFI_UP_PIN, 1);
    
  } else {
    
    flashIndicator(ACT_LED_PIN, 1);
    
  }

}


