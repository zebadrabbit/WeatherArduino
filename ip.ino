int writeWeatherData() {

  Log.notice("Writing weather data"CR);

  String output;
  root.printTo(output);
  
  WiFiClient client;
  
  if (!client.connect(host, port)) {
    
    Log.warning("Could not connect to service host."CR);
    flashIndicator(ACT_LED_PIN, 1);
    
    return -1;
    
  } else {
    
    client.print("POST /api/insert HTTP/1.1\r\n");    
    client.print("Host: 192.168.1.50:9191\r\n");
    client.print("Content-Type: application/json\r\n");
    client.print("Content-Length: ");
    client.print(root.measureLength());
    client.print("\r\n");
    client.print("Connection: close\r\n\r\n");
    client.print(output);
    client.print("\r\n");
    
  }
  
  client.stop();

  //Log.notice(output);
  flashIndicator(ACT_LED_PIN, 1);
  wifiIndicator();  
  
  return 1;
  
}

int getSyncedTime() {

  Log.notice("Getting NTP time"CR);
  wifiIndicator();  
  
  for (int cnt = 0; cnt <= 3; cnt++) {
    
    syncedTime = ntpClient.getUnixTime();
    
    if (syncedTime > 0) break;
    
    Log.warning("Could not get synced time."CR);
    flashIndicator(ACT_LED_PIN, 1);
    
  }

  if (syncedTime == 0) {
    // last successful synced time
    // deep sleep time
    // current running time
    
    Log.warning("Using last saved synced time"CR);
    
    syncedTime = lastSyncedTime + (deep_sleep_sec * 1000) + millis();
    flashIndicator(ACT_LED_PIN, 1);
    
  } else {
    
    Log.trace("Saving synced time to flash: %l"CR, syncedTime);
    
    lastSyncedTime = syncedTime;
    
  }
  
  root["timestamp"] = syncedTime;
  
}




