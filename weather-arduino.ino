/*
 * weather-arduino
 * https://gitlab.com/elukens/WeatherArduino
 * 
 * Erin Leigh Lukens
 * brocade.erin@gmail.com
 * 
 * Some code references:
 * http://cactus.io/hookups/weather/anemometer/davis/hookup-arduino-to-davis-anemometer-software
 * info@cactus.io
 * 
 * Updated project to use the nodeMCU esp32s as to minimize the need to external WIFI and
 * increase the amount of analog inputs we can use for certain sensors and allow for larger
 * program space for libraries.
 * 
 */

// esp32 libraries for wifi
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiGeneric.h>
#include <WiFiMulti.h>
#include <WiFiSTA.h>
#include <WiFiType.h>

// arduino one-wire lib for i2c
#include <Wire.h>

// get the correct time
#include <EasyNTPClient.h>
// lib for handling json data as our server requires it
#include <ArduinoJson.h>

// Adafruit developed libraries for sensor hardware.
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_SI1145.h>

// formatted logging output to serial
#include <ArduinoLog.h>

// basic process pseudo-threading.
// had to rename these as to not conflict with the ESP-BLE
// libraries during build time. 
#include <aStaticThreadController.h>
#include <aThread.h>
#include <aThreadController.h>

#include <math.h>

/*
   Setup functions
*/

/*
 * GPIO pin definitions
 * nodeMCU esp32s
 * 
 */

#define ACT_LED_PIN 27                // LED indicator (digital)
#define WIFI_DOWN_PIN 26              // LED for wifi status
#define WIFI_UP_PIN 25                // LED for wifi status
#define SPI_CS_PIN 5                  // BME280 SPI (digital CS)
#define WIND_SENSOR_PIN 16            // Anemometer pin (digital)
#define VANE_SENSOR_PIN 36            // Vane pin (analog) (Cannot use ADC2 with WIFI)
#define LOW_BATTERY_PIN 32            // Pin to detect LBO from PowerBoost

/*
 * Wifi configuration
 */
#define AP_SSID  "weather"
char ssid[] = "your-router-ssid";     //  your network SSID (name) 
char pass[] = "your-router-pass";     // your network password
int wifiStatus = 0;                   // the wifi status

ThreadController wifiControl = ThreadController();
Thread wifiTimeout = Thread();
Thread bootTimeout = Thread();

/*
 * JSON host configuration
 */
const char * host = "192.168.1.50";   // service ip/address
const uint16_t port = 9191;           // service port

/*
 * NTP configuration
 */
WiFiUDP udp;
EasyNTPClient ntpClient(udp, "pool.ntp.org", 0); // use UTC
long syncedTime = 0;
RTC_DATA_ATTR long lastSyncedTime = 0;

/*
   BME280 temp+humidity sensor
*/
#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme;

/*
 * SI1145 uv+visible light sensor
 */
 Adafruit_SI1145 uv = Adafruit_SI1145();

/*
   ArduinoJSON vars
*/
StaticJsonBuffer<200> jsonBuffer;
JsonObject& root = jsonBuffer.createObject();

/*
   Windspeed vars
*/
volatile unsigned int TimerCount = 0;
volatile unsigned long Rotations = 0;
volatile unsigned long ContactBounceTime;

/*
   Vane vars
*/
#define Offset 0;

int VaneValue;
int Direction;
int CalDirection;

/*
   Sleep vars
*/
RTC_DATA_ATTR static int boot_count = 0;
//const int deep_sleep_sec = 10 * 60;   // 10 minutes
const int deep_sleep_sec = 30;   // 30 seconds -- testing mode


void setup() {

  ++boot_count;

  wifiTimeout.enabled = true;
  wifiTimeout.setInterval(30000);
  wifiTimeout.onRun(wifiTimeoutHandler);

  bootTimeout.enabled = true;
  bootTimeout.setInterval(45000);
  bootTimeout.onRun(bootTimeoutHandler);

  pinMode(LOW_BATTERY_PIN, INPUT);
  pinMode(VANE_SENSOR_PIN, INPUT);
  pinMode(ACT_LED_PIN, OUTPUT); 
  pinMode(WIFI_DOWN_PIN, OUTPUT); 
  pinMode(WIFI_UP_PIN, OUTPUT); 
  attachInterrupt(digitalPinToInterrupt(WIND_SENSOR_PIN), isr_rotation, FALLING);

  Serial.begin(115200);
  Log.begin(LOG_LEVEL_VERBOSE, &Serial);  
  
  Log.notice("Starting up ...."CR);
  Log.trace("Boot count: %i"CR, boot_count);

  WiFi.softAPsetHostname(AP_SSID);
  WiFi.onEvent(WiFiEvent);

  flashIndicator(ACT_LED_PIN, 1);
  flashIndicator(WIFI_DOWN_PIN, 1);
  flashIndicator(ACT_LED_PIN, 3);
  
  if (!uv.begin()) {
    Log.warning("No Si1145 sensor detected. Please check wiring."CR);
  }

  if (!bme.begin()) {
    Log.warning("No BME280 sensor detected. Please check wiring."CR);
  }

  startWifi();
  
  flashIndicator(ACT_LED_PIN, 3);

  Log.notice("Entering run state ..."CR);

}

void loop() {
  // we dont need to loop. All functions are one-shot or limited retry  
}

void deepSleep() {

  Log.notice("Entering deep sleep"CR);
  esp_deep_sleep(1000000LL * deep_sleep_sec);

}

void bootTimeoutHandler() {

  Log.error("System boot timeout. Restarting device..."CR);
  delay(2000);
  ESP.restart();
  
}




