void startWifi() {

  Log.notice("WiFi starting"CR);

  wifiControl.add(&wifiTimeout);
  wifiControl.run();

  if (wifiStatus == 4) {
    Log.notice("WiFi already connected."CR);
    return;
  }
  
  // Start up Wifi and an event handler
  WiFi.begin(ssid, pass);
  
}

void WiFiEvent(WiFiEvent_t event) {

  wifiControl.add(&wifiTimeout);
  wifiControl.run();
  
  Log.notice("WiFiEvent fired"CR);
  
  switch(event) {
    case SYSTEM_EVENT_WIFI_READY:
    
      Log.notice("WIFI_READY"CR);
      
      wifiStatus = 1;
      break;
      
    case SYSTEM_EVENT_STA_CONNECTED:
    
      Log.notice("STA_CONNECTED"CR);
      
      wifiStatus = 2;
      flashIndicator(WIFI_UP_PIN, 2);
      wifiControl.clear();
      
      break;
      
    case SYSTEM_EVENT_STA_GOT_IP:
    
      Log.notice("STA_GOT_IP"CR);
      
      wifiStatus = 4;
      flashIndicator(WIFI_UP_PIN, 2);
      wifiControl.clear();

      collectSensorData();
      
      break;
      
    case SYSTEM_EVENT_STA_DISCONNECTED:
    
      Log.notice("STA_DISCONNECTED"CR);
      
      wifiStatus = 0;
      flashIndicator(WIFI_DOWN_PIN, 2);
      wifiControl.clear();
      
      break;
      
  }
    
}

void wifiTimeoutHandler() {
  
  if (wifiStatus != 4) {
    Log.notice("Wifi timeout. Resetting ..."CR);
    WiFi.disconnect();
    startWifi();
  }
  
}

