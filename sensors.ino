void collectSensorData() {

  Log.notice("Collecting sensor data"CR);
  
  checkBatteryState();
  getSyncedTime();
  getWindSpeed();
  getVaneDirection();
  getBMEData();
  getSI1145Data();

  for (int connect_cnt = 0; connect_cnt <= 3; connect_cnt++) {

    if (writeWeatherData() > 0) break;

    Log.notice("Retrying connection in 10 seconds ..."CR );
    delay(10000);
    
  }

  deepSleep();
      
}


/*
   Windspeed functions
*/
void getWindSpeed() {

  Log.notice("Getting wind speed"CR);
  
  // convert to mp/h using the formula V=P(2.25/T)
  // V = P(2.25/3) = P * 0.9
  root["wind_speed"] = Rotations * 0.9;
  Rotations = 0;
  
}

// This is the function that the interrupt calls to increment the rotation count 
void isr_rotation() { 

  if((millis() - ContactBounceTime) > 15 ) { // debounce the switch contact. 
    Rotations++; 
    ContactBounceTime = millis(); 
  } 
} 

/*
   Vane functions
*/

void getVaneDirection() {

  Log.notice("Getting vane direction"CR);

  VaneValue = analogRead(VANE_SENSOR_PIN);
  
  Direction = map(VaneValue, 0, 4095, 0, 360);
  CalDirection = Direction + Offset;

  if (CalDirection > 360)
    CalDirection = CalDirection - 360;

  if (CalDirection < 0)
    CalDirection = CalDirection + 360;

  root["vane_v"] = VaneValue;
  root["vane_d"] = CalDirection;

}

void getBMEData() {

  Log.notice("Getting bme280 data"CR);
  
  root["temp_c"] = bme.readTemperature();
  root["pressure_hpa"] = bme.readPressure() / 100;
  root["humidity"] = bme.readHumidity();

}

void getSI1145Data() {

  Log.notice("Getting si1145 data"CR);
  
  root["visible"] = uv.readVisible();
  root["ir"] = uv.readIR();
  
  float UVindex = uv.readUV();
  // the index is multiplied by 100 so to get the
  // integer index, divide by 100!
  UVindex /= 100.0;  
  
  root["uv_index"] = UVindex;  
  
}

void checkBatteryState() {

  Log.notice("Checking battery state"CR);
  flashIndicator(ACT_LED_PIN, 1);
  
  root["battery_state"] = analogRead(LOW_BATTERY_PIN);;
  
}



